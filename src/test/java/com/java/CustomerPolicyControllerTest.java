package com.java;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.java.controller.CustomerPolicyController;
import com.java.dto.CustomerPolicyDTO;
import com.java.dto.InsurancesDTO;
import com.java.service.CustomerPolicyService;

@ExtendWith(MockitoExtension.class)
public class CustomerPolicyControllerTest {

	@Mock
	CustomerPolicyService customerPolicyService;

	@InjectMocks
	CustomerPolicyController customerPolicyController;

	static CustomerPolicyDTO customerPolicyDTO;

	static InsurancesDTO insuranceDTO;

	@BeforeAll
	public static void setUp() {
		customerPolicyDTO = new CustomerPolicyDTO();
		insuranceDTO = new InsurancesDTO();
		customerPolicyDTO.setCustomerId(1);
		customerPolicyDTO.setUserAccountNumber(2334566);

	}

	@Test
	@DisplayName("Enroll insurance policy function: Positive Scenerio")
	public void customerPolicyTest() throws Exception {
		when(customerPolicyService.enrollCustomerPolicy(customerPolicyDTO)).thenReturn("customerPolicyDTO");
		String result = customerPolicyController.enrollCustomerPolicy(customerPolicyDTO);
		verify(customerPolicyService).enrollCustomerPolicy(customerPolicyDTO);
		assertEquals("customer policy registered successfully", result);
	}

}
