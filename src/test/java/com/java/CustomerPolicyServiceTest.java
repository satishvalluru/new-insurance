package com.java;

import static org.hamcrest.CoreMatchers.any;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.java.dto.CustomerDTO;
import com.java.dto.CustomerRequestDTO;
import com.java.model.Customer;
import com.java.service.CustomerService;
import com.java.service.impl.CustomerServiceImpl;

@ExtendWith(MockitoExtension.class)
public class CustomerPolicyServiceTest {

	@Mock
	CustomerService customerService;

	@InjectMocks
	CustomerServiceImpl customerServiceImpl;

	static CustomerDTO customerDTO;

	static CustomerRequestDTO customerRequestDto;

	static Customer customer;
	static Customer customerPersist;

	@BeforeAll
	public static void setUp() {
		customerDTO = new CustomerDTO();
		customerDTO.setCustomerName("jony");
		customerDTO.setStatus("success");
		customerDTO.setPolicyNumber(1);
		customerDTO.setPremiumAmount(1000.00);

		customer = new Customer();
		customer.setCustomerName("jony");
		customer.setEmail("jony@gmail.com");
		customer.setAddress("hyderabad");
		customer.setAadharId(123344);
		customer.setCustomerId(1);
		customer.setPhone("39984849");

		customerPersist = customer;
		customerPersist.setCustomerId(1);

	}

	/*
	 * @Test
	 * 
	 * @DisplayName("Save CustomerDetails") public void saveCustomerDetailsTest() {
	 * when(customerService.saveCustomer(customerRequestDto)).thenReturn(
	 * customerRequestDto); CustomerRequestDTO result =
	 * customerServiceImpl.saveCustomer(customerRequestDto);
	 * assertEquals(customerRequestDto, result);
	 * 
	 * }
	 */

	/*
	 * @Test
	 * 
	 * @DisplayName("Authenticate UserDetails : positive Scenerio") public void
	 * authenticateUserTest() throws InvalidCredentialsException {
	 * when(userDao.findByUserNameAndPassword("jony", "1234")).thenReturn(user);
	 * boolean result = userServiceImpl.authenticate("jony", "1234");
	 * assertTrue(result); }
	 * 
	 * @Test
	 * 
	 * @DisplayName("Authenticate UserDetails : negative Scenerio") public void
	 * authenticateUserTestNegative() throws InvalidCredentialsException {
	 * when(userDao.findByUserNameAndPassword("jony", "1234")).thenReturn(null);
	 * assertThrows(InvalidCredentialsException.class, () ->
	 * userServiceImpl.authenticate("jony", "1234")); }
	 */

}
