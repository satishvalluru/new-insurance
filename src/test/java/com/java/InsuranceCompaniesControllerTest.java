package com.java;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.java.controller.InsuranceCompaniesController;
import com.java.dto.InsurancesDTO;
import com.java.model.InsuranceCompanies;
import com.java.service.InsuranceCompaniesService;

@ExtendWith(MockitoExtension.class)
public class InsuranceCompaniesControllerTest {

	@Mock
	InsuranceCompaniesService insuranceCompaniesService;

	@InjectMocks
	InsuranceCompaniesController insuranceCompaniesController;


	static InsurancesDTO insuranceDTO;

	@BeforeAll
	public static void setUp() {
		insuranceDTO = new InsurancesDTO();
		insuranceDTO.setAmount(100.00);
		insuranceDTO.setInsuranceId(1);

	}

	@Test
	@DisplayName("list of insurance companies function: Positive Scenerio")
	public void insurancePolicyTest() throws Exception {
		List<InsuranceCompanies> listCompanies = new ArrayList<>();
		when(insuranceCompaniesService.listcompanies()).thenReturn(listCompanies);
		List<InsuranceCompanies> result = insuranceCompaniesController.list();
		verify(insuranceCompaniesService).listcompanies();
		assertEquals(listCompanies, result);
	}
}
