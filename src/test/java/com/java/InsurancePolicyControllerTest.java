package com.java;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.java.controller.InsurancePolicyController;
import com.java.dto.InsuranceTypeDTO;
import com.java.service.InsurancePolicyService;

@ExtendWith(MockitoExtension.class)
public class InsurancePolicyControllerTest {

	@Mock
	InsurancePolicyService insurancePolicyService;

	@InjectMocks
	InsurancePolicyController insurancePolicyController;

	static InsuranceTypeDTO insuranceTypeDTO;

	@BeforeAll
	public static void setUp() {
		insuranceTypeDTO = new InsuranceTypeDTO();
		insuranceTypeDTO.setSumInsured(100000.00);
		insuranceTypeDTO.setPeriod("2");
		insuranceTypeDTO.setInsuranceName("bajaj");

	}

	@Test
	@DisplayName("search insurance policy function: Positive Scenerio")
	public void insuranceTypeTest() throws Exception {
		List<InsuranceTypeDTO> listPolicies = new ArrayList<>();
		when(insurancePolicyService.searchInsuranceName(insuranceTypeDTO.getInsuranceName())).thenReturn(listPolicies);
		List<InsuranceTypeDTO> result = insurancePolicyController.getInsurances(insuranceTypeDTO.getInsuranceName());
		verify(insurancePolicyService).searchInsuranceName(insuranceTypeDTO.getInsuranceName());
		assertEquals(listPolicies, result);
	}
}
